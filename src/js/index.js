// import slick from 'slick-carousel';
import 'slick-carousel/slick/slick-theme.css';
import 'slick-carousel/slick/slick.css';
import axios from 'axios';

let form = document.querySelector('.form-email');
form.addEventListener('submit', (e) => {
    let api = 'https://api.unisender.com/ru/api/sendEmail?format=json&',
        apiKey = 'api_key=69dbpy6hgntzm6jjh6pem5o7eqfu84k9n7dxi6ao&',
        email = 'email=e8634738@nwytg.net&',
        senderName = 'sender_name=Oleg&',
        senderEmail = 'sender_email=info@healthempire.net&',
        subject = 'subject=TEST&',
        body = 'body=2103333&',
        listId = 'list_id=15410225',
        url = api + apiKey + email + senderName + senderEmail + subject + body + listId,
        idMail = '14402687465',
        status = 'https://api.unisender.com/ru/api/checkEmail?format=json&api_key=69dbpy6hgntzm6jjh6pem5o7eqfu84k9n7dxi6ao&email_id=' + idMail;
    e.preventDefault();
    axios.post(status, {})
        .then(function (response) {
            console.log(response);
        })
        .catch(function (error) {
            console.log(error);
        });
});
$('.cases-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    fade: true,
    cssEase: 'linear',
    arrow: true
});
let acordeionTitle = document.querySelectorAll('.acordeon__title');

acordeionTitle.forEach((elem, i) => {
    elem.addEventListener('click', (e) => {
        acordeionTitle.forEach((el, i) => {
            el.classList.remove('active');
            let elBlock = el.parentNode,
                elCont = elBlock.querySelector('.acordeon__cont');
            elCont.classList.remove('active');
        });
        elem.classList.toggle('active');

        let acordeonBlock = elem.parentNode,
            acordeonCont = acordeonBlock.querySelector('.acordeon__cont');
        acordeonCont.classList.toggle('active');
    });
});

let offsetPage = () => {
    let scrollTop = window.pageYOffset || document.documentElement.scrollTop,
        header = document.querySelector('.header'),
        casesPrice = document.querySelector('.cases-price__fixed'),
        casesPriceTop = document.querySelector('.cases__price').offsetTop;
    console.log(scrollTop);
    console.log(casesPriceTop);
    if (scrollTop > 400) {
        header.classList.add('active');

    } else {
        header.classList.remove('active');
    }
    if (scrollTop > (casesPriceTop - 60)) {
        casesPrice.classList.add('active');
    } else {
        casesPrice.classList.remove('active');
    }
};

document.onscroll = offsetPage;